from __future__ import unicode_literals

from django.db import models
from django.db.models import Avg, Max
import math, itertools, re

# Create your models here.
class Hotel(models.Model):
    name = models.CharField(max_length=100)
    testing = models.BooleanField(default=False)

    def get_avg(self):
        avg = self.review_set.all().aggregate(Avg('overall'))
        if avg['overall__avg'] > 0:
            n = avg['overall__avg']
            return '%.1f' % round(n, 1)
        else: return None

    def get_stars(self):
        avg = float(self.get_avg())
        if avg >= 0:
            avg = int(avg*2)
            avg = float(avg) / 2
            full = int(avg)
            half = int(math.ceil(avg - full))
            empty = 5 - full - half;
            return {"full_stars":full,"half_star":half,"empty_stars":empty}
        else: return None

    def get_image_name(self):
        number = self.name[-1]
        return "exterior"+number+".jpg"

    def count_reviews(self):
        return len(self.review_set.all())

    def get_best_review(self):
        best = self.review_set.order_by('-overall')
        if len(best) > 0:
            return best[0]
        else: return None

    def get_worse_review(self):
        worse = self.review_set.order_by('overall')
        if len(worse) > 0:
            return worse[0]
        else: return None

    def __str__(self):
        return self.name

class Review(models.Model):
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    testing = models.BooleanField(default=False)
    content = models.TextField()
    overall = models.IntegerField()

    def get_stars(self):
        avg = float(self.overall)
        if avg >= 0:
            avg = int(avg*2)
            avg = float(avg) / 2
            full = int(avg)
            half = int(math.ceil(avg - full))
            empty = 5 - full - half;
            return {"full_stars":full,"half_star":half,"empty_stars":empty}
        else: return None

class ModelAccuracy(models.Model):
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    success = models.IntegerField(default=0)
    total_reviews = models.IntegerField(default=0)

    @property
    def success_ratio(self):
        if self.total_reviews > 0:
            return float(self.success) / float(self.total_reviews)
        else: return -1

class Term(models.Model):
    name = models.CharField(max_length=200)
    frequency = models.IntegerField(default=1)

    # Returns the top 1000 terms near to the term frequency average
    @staticmethod
    def get_dictionary():
        max_freq = Term.objects.all().aggregate(Max("frequency"))
        freq = max_freq["frequency__max"]*0.8
        pivot = str(freq)

        # 350 above pivot
        above = Term.objects.all().extra(where=["frequency >= "+pivot]).order_by("frequency")[:350]
        # 350 below pivot
        below = Term.objects.all().extra(where=["frequency < "+pivot]).order_by("-frequency")[:350]
        dictionary = set(itertools.chain(above, below))
        return dictionary

    def __str__(self):
        return self.name

class Skeleton(models.Model):
    managed = False

    @staticmethod
    def get_skeleton():
        dictionary = Term.get_dictionary()
        vertexes = ["overall".encode("utf-8")]
        edges = []
        for term in dictionary:
            vertexes.append(term.name.encode("utf-8"))
            edges.append(["overall".encode("utf-8"), term.name.encode("utf-8")])
        skeleton = {"V": vertexes, "E": edges}
        return skeleton

class BnetData(models.Model):
    managed = False

    @staticmethod
    def get_data():
        dictionary = Term.get_dictionary()
        reviews = Review.objects.all()[:10000]
        data = []
        for review in reviews:
            rdict = {"overall".encode("utf-8"): review.overall}
            for term in dictionary:
                matches = re.findall('\\b'+term.name+'\\b', review.content, flags=re.IGNORECASE)
                if len(matches)>0:
                    rdict[term.name.encode("utf-8")] = 1
                else:
                    rdict[term.name.encode("utf-8")] = 0
            data.append(rdict.copy())

        # workaround per assicurarsi che PGMLearner trovi
        # tutti i possibili valori assunti da un vertice
        for i in range(0,2):
            rdict = {"overall".encode("utf-8"): -1}
            for term in dictionary:
                rdict[term.name.encode("utf-8")] = i
            data.append(rdict.copy())

        return data

class BnetSettings(models.Model):
    key = models.TextField(unique=True)
    value = models.TextField()

    def __str__(self):
        return self.key
