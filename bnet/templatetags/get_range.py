from django.template import Library

register = Library()

@register.filter
def get_range( value ):
    if value is not None:
        return range( value )
    else: return range( 0 )
