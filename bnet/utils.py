import re, os, sys
from os import listdir
from os.path import isfile, join
from json import dumps as json_write
from json import loads as json_parse
from .models import *
from stemming.porter2 import stem
from stop_words import get_stop_words
from libpgm.discretebayesiannetwork import *
from libpgm.pgmlearner import *
from libpgm.nodedata import *
from libpgm.graphskeleton import *
from libpgm.tablecpdfactorization import *

class BnetUtils:
    RE_REVIEWS = re.compile("\r\n\r\n", re.UNICODE)
    RE_METATAGS = re.compile("\r\n", re.UNICODE)
    RE_METATAG = re.compile("<(.+)>(.*)", re.UNICODE)
    RE_METATAG_CONTENT = re.compile("<(Content)>(.*)\r\n", re.UNICODE)
    RE_METATAG_OVERALL = re.compile("<(Overall)>(.*)\r\n", re.UNICODE)
    RE_TERMS = re.compile("([A-Za-z]+)\s*")

    @staticmethod
    def dataset_split_reviews(filename):
        with open(filename, 'r') as dataset:
            data = dataset.read()
        reviews = BnetUtils.RE_REVIEWS.split(data)
        return reviews

    @staticmethod
    def review_split_metatags(review):
        metatags = BnetUtils.RE_METATAGS.split(review)
        return metatags

    @staticmethod
    def review_filter_sentiment_metatags(review):
        content = BnetUtils.RE_METATAG_CONTENT.split(review)
        overall = BnetUtils.RE_METATAG_OVERALL.split(review)
        if 2 < len(content):
            content = content[2]
        else: content = "";
        if 2 < len(overall):
            overall = overall[2]
        else: overall = -1
        return {'content': content, 'overall': overall}

    @staticmethod
    def review_parse_metatag(metatag):
        result = BnetUtils.RE_METATAG.split(metatag)
        if len(result) == 4:
            return {result[1]: result[2]}
        else: return {}

    @staticmethod
    def review_parse_content(metatag):
        return 1

    @staticmethod
    def merge_dict(a, b):
        c = a.copy()
        c.update(b)
        return c

    @staticmethod
    def parse_dataset(filename):
        out = []
        reviews = BnetUtils.dataset_split_reviews(filename)
        for rev in reviews:
            metatags = BnetUtils.review_split_metatags(rev)
            review = {}
            for tag in metatags:
                metatag = BnetUtils.review_parse_metatag(tag)
                review = BnetUtils.merge_dict(review, metatag)
            out.append(review)
        return out

    @staticmethod
    def parse_dataset_sentiment(filename, hotel, test=False):
        out = []
        reviews = BnetUtils.dataset_split_reviews(filename)
        for rev in reviews:
            review = BnetUtils.review_filter_sentiment_metatags(rev)
            hotel_review = hotel.review_set.create(content=review['content'], overall=review['overall'], testing=test)
            out.append(review)
        hotel.save()
        return out

    @staticmethod
    def test_model_accuracy(dirpath, filename):
        file_dir = os.path.dirname(__file__)
        PROJECT_ROOT = os.path.abspath(file_dir)
        PATH = os.path.join(PROJECT_ROOT, dirpath, filename)
        result = []
        data = []
        nd = NodeData()
        nd.load("nodedata.txt")
        gs = GraphSkeleton()
        gs.load("nodedata.txt")
        gs.toporder()
        dictionary = Term.get_dictionary()
        reviews = BnetUtils.dataset_split_reviews(PATH)
        for rev in reviews:
            review = BnetUtils.review_filter_sentiment_metatags(rev)
            evidence = dict()
            # words = re.sub(ur"[^A-Za-z\s]", ' ', review["content"])
            # words = re.sub(ur" +", ' ', words)
            # words = words.encode("utf-8")
            # words = re.compile(r'\s+').split(words)
            # words = BnetUtils.process_testing_terms(words)
            # content = " ".join(words)
            #print(review["content"])
            for term in dictionary:
                matches = re.findall('\\b'+term.name+'\\b', review["content"], flags=re.IGNORECASE)
                if len(matches)>0:
                    evidence[term.name.encode("utf-8")] = 1
                else:
                    evidence[term.name.encode("utf-8")] = 0
            values = range(-1,6)
            results = dict()
            for val in values:
                query = dict(overall=[val])
                bn = DiscreteBayesianNetwork(gs, nd)
                fn = TableCPDFactorization(bn)
                result = fn.specificquery(query, evidence)
                results[val] = result
            est_value = max(results, key=results.get)
            compare = dict(estimate=est_value,real=int(review["overall"]))
            if abs(compare["estimate"]-compare["real"])<=1:
                data.append(1)
            else: data.append(0)
        success = data.count(1)
        total = len(data)
        if total > 0:
            ratio = float(success)/float(total)*100
        else: ratio = "?"
        return "Accuracy: "+str(ratio)+"%"

    @staticmethod
    def test_hotel_model_accuracy(hotel):
        data = []
        m = ModelAccuracy(hotel=hotel)
        nd = NodeData()
        nd.load("nodedata.txt")
        gs = GraphSkeleton()
        gs.load("nodedata.txt")
        gs.toporder()
        dictionary = Term.get_dictionary()
        reviews = hotel.review_set.all()
        for review in reviews:
            m.total_reviews += 1
            evidence = dict()
            for term in dictionary:
                matches = re.findall('\\b'+term.name+'\\b', review.content, flags=re.IGNORECASE)
                if len(matches)>0:
                    evidence[term.name.encode("utf-8")] = 1
                else:
                    evidence[term.name.encode("utf-8")] = 0
            values = range(-1,6)
            results = dict()
            for val in values:
                query = dict(overall=[val])
                bn = DiscreteBayesianNetwork(gs, nd)
                fn = TableCPDFactorization(bn)
                result = fn.specificquery(query, evidence)
                results[val] = result
            est_value = max(results, key=results.get)
            compare = dict(estimate=est_value,real=int(review.overall))
            if abs(compare["estimate"]-compare["real"])<=1:
                m.success += 1
        m.save()
        return m

    @staticmethod
    def test_all_hotels():
        hotels = Hotel.objects.filter(testing=True)
        total = hotels.count()
        idx = 1
        for hotel in hotels:
            BnetUtils.test_hotel_model_accuracy(hotel)
            sys.stdout.write("    Completato: %i/%i   \r" % (idx,total))
            sys.stdout.flush()
            idx += 1
        return "Elaborazione completata con successo."

    @staticmethod
    def parse_all_datasets(dirpath, test=False):
        out = []
        file_dir = os.path.dirname(__file__)
        PROJECT_ROOT = os.path.abspath(file_dir)
        DIR_PATH = join(PROJECT_ROOT, dirpath)
        files = [f for f in listdir(DIR_PATH) if isfile(join(DIR_PATH, f))]
        for f in files:
            if f.endswith('.dat'):
                PATH = os.path.join(PROJECT_ROOT, dirpath, f)
                hotel_name = f.replace("_", " ").replace(".dat", "")
                hotel = Hotel(name=hotel_name,testing=test)
                hotel.save()
                hotel_data = BnetUtils.parse_dataset_sentiment(PATH, hotel)
                hotel = {'name': f, 'data': hotel_data}
                out.append(hotel)
        return out

    @staticmethod
    def process_terms(terms):
        stop_words = get_stop_words('english')
        for idx, term in enumerate(terms):
            terms[idx] = stem(term)
            if term in stop_words:
                del terms[idx]
            sys.stdout.write("    Completato: %i/%i   \r" % (idx,len(terms)))
            sys.stdout.flush()
        terms = list(set(terms))
        for term in terms:
            cached_term = Term.objects.filter(name=term)
            if len(cached_term) < 1:
                Term(name=term).save()
            else:
                t = cached_term[0]
                t.frequency += 1
                t.save()
        return terms

    @staticmethod
    def process_testing_terms(terms):
        words = []
        unique_terms = list(set(terms))
        stop_words = get_stop_words('english')
        for term in unique_terms:
            word = stem(term)
            if word.lower() not in stop_words:
                words.append(word)
        return words

    @staticmethod
    def extract_terms():
        hotels = Hotel.objects.all()
        tot = Hotel.objects.count()
        terms = 0
        for hid,hotel in enumerate(hotels):
            sys.stdout.write("    Hotel: %i/%i   \r" % (hid,tot))
            sys.stdout.flush()
            reviews = hotel.review_set.all()
            for review in reviews:
                words = re.sub(ur"[^A-Za-z\s]", ' ', review.content)
                words = words.encode("utf-8")
                words = re.compile(r'\s+').split(words)
                BnetUtils.process_terms(words)
                terms = terms + len(words)
        return terms

    @staticmethod
    def tester():
        tot = 100000
        for i in range(0,tot):
            sys.stdout.write("    Completato: %i/%i   \r" % (i,tot))
            sys.stdout.flush()
