from django.conf.urls import url

from . import views

app_name = 'bnet'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^all-hotels/$', views.all_hotels, name='all_hotels'),
    url(r'^hotels/$', views.HotelListView.as_view(), name='hotel_list'),
    url(r'^hotels/testing/$', views.test_hotel_list, name='testing_hotel_list'),
    url(r'^hotels/testing/(?P<pk>[0-9]+)/$', views.testing_hotel_details, name='testing_hotel_detail'),
    url(r'^hotels/(?P<pk>[0-9]+)/$', views.hotel_details, name='hotel_detail'),
    url(r'^hotels/sentiment/$', views.estimate_sentiment, name='estimate_sentiment')
]
