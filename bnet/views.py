import os, re
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.core.urlresolvers import reverse
from django.views import generic
from django.db.models import Avg
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from libpgm.discretebayesiannetwork import *
from libpgm.pgmlearner import *
from libpgm.nodedata import *
from libpgm.graphskeleton import *
from libpgm.tablecpdfactorization import *

from .utils import BnetUtils
from .models import *

# Create your views here.
def index(request):
    hotels = Hotel.objects.filter(testing=False).count()
    reviews = Review.objects.filter(testing=False).count()
    counter = []
    for i in range(1,6):
        r = Review.objects.filter(overall=i).count()
        counter.append(r)

    terms = Term.objects.count()
    used_terms = len(Term.get_dictionary())
    performances = ModelAccuracy.objects.all()
    total_reviews = 0
    success = 0
    for p in performances:
        total_reviews += p.total_reviews
        success += p.success
    if total_reviews > 0:
        ratio = float(success) / float(total_reviews) * 100
    else: ratio = -1

    return render(request, "bnet/index.html", {
        'hotels': hotels,
        'reviews': reviews,
        'terms': terms,
        'used_terms': used_terms,
        'ratio': ratio,
        'testing_reviews': total_reviews,
        'total_reviews': total_reviews+reviews,
        'counter': counter,
        'nav_active': "index"})

def all_hotels(request):
    hotels = BnetUtils.parse_all_datasets("datasets/training")
    return render(request, "bnet/all-hotels.html", {'hotels': hotels})

def hotel_details(request, pk):
    hotel = get_object_or_404(Hotel, pk=pk)
    review_list = hotel.review_set.all()
    paginator = Paginator(review_list, 25) # Show 25 reviews per page

    page = request.GET.get('page')
    try:
        reviews = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        reviews = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        reviews = paginator.page(paginator.num_pages)


    return render(request, 'bnet/hotel-detail.html', {
        "hotel":hotel,
        "review_list":reviews,
        "page_obj":paginator,
        "nav_active":"hotel_detail"})

def testing_hotel_details(request, pk):
    hotel = get_object_or_404(Hotel, pk=pk)
    review_list = hotel.review_set.all()
    accuracy = hotel.modelaccuracy_set.all()[0]
    success_ratio = accuracy.success_ratio * 100
    paginator = Paginator(review_list, 25) # Show 25 reviews per page

    page = request.GET.get('page')
    try:
        reviews = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        reviews = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        reviews = paginator.page(paginator.num_pages)


    return render(request, 'bnet/test-hotel-detail.html', {
        "hotel":hotel,
        "success_ratio": success_ratio,
        "review_list":reviews,
        "page_obj":paginator,
        "nav_active":"hotel_detail"})

def estimate_sentiment(request):
    nd = NodeData()
    nd.load("nodedata.txt")
    gs = GraphSkeleton()
    gs.load("nodedata.txt")
    gs.toporder()
    dictionary = Term.get_dictionary()
    content = request.POST.get('content', '')
    review = dict()
    # review["content"] = u"Business hotel that also handles vacations Long weekend that was made all the more memorable with an attentive and courteous staff. Great location. Stayed in the North tower with a room overlooking the bay. Others have commented about dated rooms but this one was clean. Do park next door for $18.00. There's a catwalk to the second floor of the hotel. $38.00 a day for self park is a little much even for Seattle."
    # review["overall"] = 2
    # words = re.sub(ur"[^A-Za-z\s]", ' ', review["content"])
    # words = re.sub(ur" +", ' ', words)
    # words = words.encode("utf-8")
    # words = re.compile(r'\s+').split(words)
    # words = BnetUtils.process_testing_terms(words)
    # #words = map(str.lower, words)
    # words = list(set(words))
    evidence = dict()
    for term in dictionary:
        matches = re.findall('\\b'+term.name+'\\b', content, flags=re.IGNORECASE)
        if len(matches)>0:
            evidence[term.name.encode("utf-8")] = 1
        else:
            evidence[term.name.encode("utf-8")] = 0
    values = range(-1,6)
    results = dict()
    for val in values:
        query = dict(overall=[val])
        bn = DiscreteBayesianNetwork(gs, nd)
        fn = TableCPDFactorization(bn)
        result = fn.specificquery(query, evidence)
        results[val] = result
    est_value = max(results, key=results.get)
    return JsonResponse({"estimate": est_value})

def test_hotel_list(request):
    hotels = Hotel.objects.filter(testing=True)
    paginator = Paginator(hotels, 10) # Show 10 hotels per page

    page = request.GET.get('page')
    try:
        hotel_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        hotel_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        hotel_list = paginator.page(paginator.num_pages)

    return render(request, 'bnet/test-hotel-list.html', {
        "hotel_list":hotel_list,
        "page_obj":paginator,
        "nav_active":"testing_hotel_list"})

class HotelListView(generic.ListView):
    model = Hotel
    template_name = "bnet/hotel-list.html"
    paginate_by = 10
    def get_avg(hotel_id):
        return Hotel.objects.filter(pk=hotel_id).review_set.all().aggregate(Avg('overall'))
    def get_testing_hotels():
        return Hotel.objects.filter(testing=True)
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(HotelListView, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['nav_active'] = "hotel_list"
        return context

class HotelDetailView(generic.DetailView):
    model = Hotel
    template_name = "bnet/hotel-detail.html"
